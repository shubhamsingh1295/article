class ApiIndexRenderer
  attr_reader :name, :link_ref, :method_type

  def initialize(name, link_ref, method_type)
    @name        = name
    @link_ref    = link_ref
    @method_type = method_type
  end
  
  class << self
    
    def user_api_index
      index_arr = []
      index_arr << ApiIndexRenderer.new("Sign up", "sign up", "POST")
      index_arr << ApiIndexRenderer.new("Login", "login", "POST")
      index_arr << ApiIndexRenderer.new("Logout", "logout", "GET")                        
      index_arr << ApiIndexRenderer.new("Create", "create", "POST")                       
      index_arr << ApiIndexRenderer.new("Update", "update", "POST")                         
      index_arr << ApiIndexRenderer.new("Show", "show", "GET")                         
      index_arr << ApiIndexRenderer.new("Index", "index", "GET")                
      index_arr << ApiIndexRenderer.new("Destroy", "destroy", "DELETE")                                  
      index_arr
    end     
  end
end