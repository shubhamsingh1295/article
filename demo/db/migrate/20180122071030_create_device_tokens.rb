class CreateDeviceTokens < ActiveRecord::Migration
  def change
    create_table :device_tokens do |t|
      t.string :device_token
      t.references :user, index: true, foreign_key: true
      t.references :auth_token, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
