class CreateArticleTasks < ActiveRecord::Migration
  def change
    create_table :article_tasks do |t|
      t.string :name
      t.string :title
      t.string :url

      t.timestamps null: false
    end
  end
end
