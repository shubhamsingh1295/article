class AddColumnToAuthenticationToken < ActiveRecord::Migration
  def change
    add_column :authentication_tokens, :references, :user
  end
end
