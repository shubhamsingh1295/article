class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
has_many :auth_tokens, :dependent => :destroy
has_many :article_tasks, :dependent => :destroy
has_many :device_tokens, :dependent => :destroy
class << self
	def find_user_from_authentication_token(token)
      u = User.where(auth_token: token)
       u = where(auth_token:token).includes(:user)
      (u.present? && u.first.present?)? u.first : nil
    end
     
    def authenticate_user_with_auth(email, password)
     return nil unless email.present? or password.present?
     u = User.find_by_email(email)
     (u.present? && u.valid_password?(password))? u : nil
    end

	def invalid_credentials
	  "Email or Password is not valid"
	end
end              
end
