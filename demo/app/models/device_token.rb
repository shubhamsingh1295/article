class DeviceToken < ActiveRecord::Base
  belongs_to :user
  belongs_to :auth_token
end
