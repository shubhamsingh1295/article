json.result do
  json.messages  "ok"
  json.rstatus   "1"
  json.errorcode ""
end
json.data do
  	json.articles @articles.each do |a|	
  	  json.id	a.id 
	  json.name a.name
	  json.title a.title
	  json.url a.url
	end  
end 