json.result do
  json.messages  "ok"
  json.rstatus   "1"
  json.errorcode ""
end
json.data do
  json.id @article.id	 
  json.name @article.name
  json.title @article.title
  json.url @article.url
end 