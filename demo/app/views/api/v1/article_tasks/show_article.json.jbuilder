json.result do
  json.messages  "ok"
  json.rstatus   "1"
  json.errorcode ""
end
json.data do 
  json.id @article.try(:id)		
  json.name @article.try(:name)
  json.title @article.try(:title)
  json.url @article.try(:url)
end 