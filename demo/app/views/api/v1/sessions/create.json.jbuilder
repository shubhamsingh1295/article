json.result do
  json.messages  "ok"
  json.rstatus   "1"
  json.errorcode ""
end
json.data do 
  json.email @user.email  
  json.extract! @auth_token, :auth_token
  json.device_token @device_token.try(:device_token)
end 