class Api::V1::ArticleTasksController < Api::V1::BaseController	

	def index
		@user = AuthToken.find_by_auth_token(params[:auth_token])
		if @user.present?
		   @articles = ArticleTask.all
		else
		   render_json({:errors => "No user found with authentication_token = #{params[:auth_token]}"}.to_json)	
		end	    
	end

	def new
		@article = ArticleTask.new
	end

	def create
		@user = AuthToken.find_by_auth_token(params[:auth_token])
		if @user.present?
			@article = ArticleTask.create(create_params)
			@article.user_id = @user.user_id
			if @article.save
				# it will redirect ot view page
			else
				render_json({:result=>{:messages => "Something went wrong",:rstatus=>0, :errorcode => 404}}.to_json)	
			end
		else
			render_json({:errors => "No user found with authentication_token = #{params[:auth_token]}"}.to_json)
		end	
	end	

	def show_article
			@user = AuthToken.find_by_auth_token(params[:auth_token])
			if @user.present?			
				  @article = ArticleTask.find_by(id: params[:article_task][:id])
				  if @article.present?
				
				  else
				  	render_json({:errors => "No article found with user id = #{params[:article_task][:id]}"}.to_json)
					end
			else
				 render_json({:errors => "No user found with authentication_token = #{params[:auth_token]}"}.to_json)
			end	 					
	end

	def update_article
		@user = AuthToken.find_by_auth_token(params[:auth_token])
		if @user.present?
			 @article = ArticleTask.find_by(id: params[:article_task][:id])
			if @article.present?
					if @article.update_attributes(create_params)
					
					else
							render_json({:result=>{:messages => "Something went wrong",:rstatus=>0, :errorcode => 404}}.to_json)	
					end
			else
					render_json({:errors => "No article found with this ID = #{params[:article_task][:id]}"}.to_json)			
			end
		else	
			 render_json({:errors => "No user found with authentication_token = #{params[:auth_token]}"}.to_json)
		end				
	end

	def destroy_article
		@user = AuthToken.find_by_auth_token(params[:auth_token])
		if @user.present?
			 @article = ArticleTask.find_by(id: params[:article_task][:id])
			  if @article.present?
			 		@article.destroy
			 			render_json({:result=>{:messages =>"ok",:rstatus=>1, :errorcode =>""},:data=>{:messages =>"Article Deleted Successfully!"}}.to_json)
				else
					  render_json({:errors => "No article found with this ID = #{params[:article_task][:id]}"}.to_json)			
				end
		else
				render_json({:errors => "No user found with authentication_token = #{params[:auth_token]}"}.to_json)
		end		
	end

	private

		def create_params
			params.require(:article_task).permit(:name, :title, :url, :user_id)
		end
end
