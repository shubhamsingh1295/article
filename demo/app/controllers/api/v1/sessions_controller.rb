class Api::V1::SessionsController < Api::V1::BaseController

  def sign_up
    @user = User.new(user_create_params)
    if @user.save 
    else  
      render_json({:result=>{:messages => "Something went wrong", :rstatus=>0, :errorcode => 404}}.to_json)
    end
  end


  def create
     @user = User.authenticate_user_with_auth(params[:email], params[:password])
    if @user.present?
       @device_token = DeviceToken.create(create_params)
       @device_token.user_id = @user.id
       @device_token.save
       @auth_token = @user.auth_tokens.create(:auth_token => AuthToken.generate_unique_token)
    else
        render_json({:result=>{:messages => User.invalid_credentials,:rstatus=>0, :errorcode => 404}}.to_json)
    end    
  end

  def destroy
    @user = AuthToken.find_by_auth_token(params[:auth_token])
    @device_token = DeviceToken.find_by(device_token: params[:device_token][:device_token])
    if @user.present?
      if @device_token.present?
         @user.destroy
         render_json({:result=>{:messages =>"ok",:rstatus=>1, :errorcode =>""},:data=>{:messages =>"Logout Successfully!"}}.to_json)
      else
         render_json({:errors => "No user found with device_token = #{params[:device_token][:device_token]}"}.to_json)
      end
    else
      render_json({:errors => "No user found with authentication_token = #{params[:auth_token]}"}.to_json)
    end
  end

  private
    def create_params
       params.require(:device_token).permit(:device_token, :user_id)
    end

    def user_create_params
      params.require(:user).permit(:email, :password)
    end
end