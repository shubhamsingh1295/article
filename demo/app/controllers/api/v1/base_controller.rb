class Api::V1::BaseController < ActionController::Base

  protected

  def render_json(json)
     callback = params[:callback]
     response = begin
       if callback
         "#{callback}(#{json});"
       else
         json
       end
     end
     render({:content_type => :js, :text => response})
   end
end
